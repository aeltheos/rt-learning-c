#include <stdio.h>
#include <stdlib.h>

#include <cglm/euler.h> // I really need to learn quaternions.
#include <cglm/vec3.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb/stb_image_write.h"


typedef struct Ray {
  vec3 pos;
  vec3 dir;
} Ray;

Ray gen_camera_ray (vec3 pos, vec3 angle, unsigned int i, unsigned int j, unsigned int i_max, unsigned int j_max, float depth) {
  Ray ray;

  glm_vec3_copy(pos, ray.pos);
  
  ray.dir[0] = ((float) ((int) i - (int) i_max / 2) / (float) j_max);
  ray.dir[1] = ((float) ((int) j - (int) j_max / 2) / (float) j_max);
  ray.dir[2] = depth;

  mat4 rot;
  glm_euler(angle, rot);
  glm_vec3_rotate_m4(rot,ray.dir, ray.dir);

  glm_vec3_normalize(ray.dir);

  return ray;
}

typedef struct AlignedBox{
  vec3 lb; // minimal corner
  vec3 rt; // maximal corner
} AlignedBox;

float intersect_ray_box(AlignedBox box, Ray ray) {
  // Adapted from https://gamedev.stackexchange.com/a/18459

  // Compute direction inverse.
  vec3 dirfrac = {1.0 / ray.dir[0], 1.0 / ray.dir[1], 1.0 / ray.dir[2]};
  // lb is the corner of AABB with minimal coordinates - left bottom, rt is maximal corner
  // r.org is origin of ray
  float t1 = (box.lb[0] - ray.pos[0])*dirfrac[0];
  float t2 = (box.rt[0] - ray.pos[0])*dirfrac[0];
  float t3 = (box.lb[1] - ray.pos[1])*dirfrac[1];
  float t4 = (box.rt[1] - ray.pos[1])*dirfrac[1];
  float t5 = (box.lb[2] - ray.pos[2])*dirfrac[2];
  float t6 = (box.rt[2] - ray.pos[2])*dirfrac[2];

  float tmin = fmax(fmax(fmin(t1, t2), fmin(t3, t4)), fmin(t5, t6));
  float tmax = fmin(fmin(fmax(t1, t2), fmax(t3, t4)), fmax(t5, t6));

  if (tmax < 0 || tmin > tmax) {
    return INFINITY;
  }
  return tmin;
}

int main(int argc, char **argv) {
  unsigned int width = 1080, height = 720, channels = 3;
  unsigned int img_size = width * height * channels;
  unsigned char *img = malloc(img_size);

  AlignedBox box = {
    .lb = {-0.5, -0.5, 2.0},
    .rt = {0.5, 0.5, 3.0}
  };

  for (unsigned int j = 0; j < height; j++) {
    for (unsigned int i = 0; i < width; i++) {
      unsigned char *p = img + (channels * (j * width + i));

      vec3 pos = {-0.7, 0.0, 0.0};
      vec3 dir = {0.0, 0.0, 0.0};
      Ray ray = gen_camera_ray(pos, dir, i, j, width, height, 1.0);

      float t = intersect_ray_box(box, ray);

      if (t!=INFINITY) {
        *p = (char) (255 * t * 0.2);
        *(p+1) = (char) (255 * t * 0.2);
        *(p+2) = (char) (255 * t * 0.2);
      } else {
        *p = 0;
        *(p+1) = 0;
        *(p+2) = 0;
      }
    }
  }

  stbi_write_jpg("img.jpeg", width, height, channels, img, 100);
  free(img);

  return 0;
}
