{pkgs ? import <nixpkgs> {} }:
let
in pkgs.mkShell {
  nativeBuildInputs = with pkgs; [
    gcc
    stb
    cglm
    clang-format
  ];
}
